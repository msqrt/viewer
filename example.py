
import numpy as np
import torch
import threading
import imgui
import glfw

from viewer import viewer

# This function will be called by the viewer for every frame.
# v is a reference to the viewer itself (the footprint has to be this)
def loop(v):
	# Each imgui begin..end pair introduces a window.
	imgui.begin('Plasma')
	v.draw_image('pl') # draws an uploaded torch_tensor (skips if image not found)
	imgui.text('try pressing space!')
	imgui.end()
	
	imgui.begin('Raymarch')
	imgui.text(v.message)
	v.scale = imgui.slider_float('Size', v.scale, .5, 2.)[1]
	v.draw_image('rm', scale = v.scale)
	v.speed = imgui.slider_float('Speed', v.speed, .0, 1.)[1]
	imgui.end()


from time import perf_counter


def draw(v, device):
	t = .0
	xs = torch.linspace(.0, 256., 256, device=device)
	ys = torch.linspace(.0, 256., 256, device=device)
	m = torch.stack([xs[None].repeat(256,1), ys[:,None].repeat(1,256)],-1)
	while not v.quit:
		start = perf_counter()
		plasma = t+.07*((m[:,:,0]/16.).sin()+(m[:,:,1]/32.).sin()+(((m[:,:,0]-128.)**2+(m[:,:,1]-128.)**2).sqrt()/8.).sin()+(m.norm(dim=2)/8.).sin())

		# upload_image takes a name and a torch tensor
		v.upload_image('pl', torch.stack([.5+.5*(3.1415*plasma*16.).sin(), .5+.5*(3.1415*plasma*4.).sin(), .5+.5*(3.1415*plasma*2.).sin()],2))
		
		# you can query keys with keydown and keyhit
		t += .2*(perf_counter()-start) * (-1. if v.keydown(glfw.KEY_SPACE) else 1.)

def also_draw(v, device, text):
	t = .0
	v.message = text
	def sdf(x):
		return .7-((x-x.floor())-torch.tensor([.5, .5, .5], device=device)[None,None]).norm(dim=2)
	xs = torch.linspace(-.5, .5, 256, device=device)
	ys = torch.linspace(-.5, .5, 256, device=device)
	m = torch.stack([xs[None].repeat(256,1), ys[:,None].repeat(1,256)],-1)
	while not v.quit:
		start = perf_counter()
		orig = torch.ones(256, 256, 3, device = device)*.5
		orig[:,:,2] += t
		ray = torch.cat([m, torch.ones(256,256,1,device=device)],2)
		ang = -t*.2
		ray = torch.stack([ray[:,:,0]*np.cos(ang)+ray[:,:,2]*np.sin(ang), ray[:,:,1], ray[:,:,2]*np.cos(ang)-ray[:,:,0]*np.sin(ang)],2)
		ray = ray/ray.norm(dim=2,keepdim=True)
		pos = orig.clone()
		for _ in range(20):
			pos += ray * sdf(pos)[:,:,None]

		v.upload_image('rm', (-(pos-orig).norm(dim=2)).exp())
		end = perf_counter()
		v.message = '{:3f}s'.format(end-start)

		v.editable('rm_code', **locals()) # this opens an editor window where you can live-add new visualizations.
		t += v.speed*(end-start)

if __name__=='__main__':

	device = torch.device('cuda')
	v = viewer('Example')
	# here we can initialize variables that need to be used both by the loop function and the worker threads
	# don't use names that begin with an undescore; these are used by the inner workings of viewer.
	v.speed = .5
	v.scale = 1.
	v.message = ''

	# initialize any number of threads
	thread_1 = threading.Thread(target=draw, args=(v,device)) # target is the function to run
	thread_2 = threading.Thread(target=also_draw, args=(v, device, 'passing something else'))

	# run! first arg is the loop function, the second is a list/tuple of threads.
	v.start(loop, (thread_1, thread_2))
