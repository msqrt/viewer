﻿viewer
------

![alt text](https://users.aalto.fi/kemppip2/viewer_example_screenshot.png "Now your software, too, can be cool like this!")

This is a simple GUI wrapper for pyimgui and pytorch.

The two key features are:

* visualize heavy computations without the UI lagging
* display torch tensors without copying them through main memory (that is, the data stays on the GPU)

The installation requires pycuda -- for windows, you can get a ready-made wheel from https://www.lfd.uci.edu/~gohlke/pythonlibs/#pycuda, for linux/mac it's easiest to build your own following the instructions at https://wiki.tiker.net/PyCuda/Installation/.
After you have the pycuda wheel, place it in the same directory as environment.yml, edit the pycuda line in environment.yml to match the wheel and execute 'conda env create -f environment.yml' -- or fetch the listed packages with whatever environment management system you prefer.